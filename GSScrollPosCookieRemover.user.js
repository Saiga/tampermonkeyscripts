// ==UserScript==
// @name         GSScrollPosKiller
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  deletes GreatSuspender cookies on youtube
// @author       Saiga
// @updateURL    https://gitlab.com/Saiga/tampermonkeyscripts/raw/master/GSScrollPosCookieRemover.user.js
// @downloadURL  https://gitlab.com/Saiga/tampermonkeyscripts/raw/master/GSScrollPosCookieRemover.user.js
// @match        *://www.youtube.com/*
// @grant        none
// @run-at       document-start
// ==/UserScript==

(function() {
    var res = document.cookie;
    var multiple = res.split(";");
    var re = new RegExp("gsScrollPos.*");
    for(var i = 0; i < multiple.length; i++) {
        var key = multiple[i].split("=");
        if (re.test(key[0])) {
            document.cookie = key[0]+" =; expires = Thu, 01 Jan 1970 00:00:00 UTC";
        }
    }
})();