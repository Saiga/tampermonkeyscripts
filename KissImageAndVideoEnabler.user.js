// ==UserScript==
// @name         Kiss Image and Video Enabler
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  enables hidden Video and Img blocks on Kiss sites
// @author       Saiga
// @updateURL    https://gitlab.com/Saiga/tampermonkeyscripts/raw/master/KissImageAndVideoEnabler.user.js
// @downloadURL  https://gitlab.com/Saiga/tampermonkeyscripts/raw/master/KissImageAndVideoEnabler.user.js
// @match        *://kimcartoon.me/*
// @match        *://kissmanga.com/*
// @grant        none
// ==/UserScript==

(function() {
    var vidElm = document.getElementById('my_video_1');
    if (vidElm === null) {
        return;
    }
    vidElm.style.display = 'block';
    if (vidElm.hasChildNodes('video')) {
        vidElm.getElementsByTagName('video').item(0).style.display = 'block';
    }
})();

(function() {
    var imgElm = document.getElementById('divImage');
    if (imgElm === null) {
        return;
    }
    imgElm.style.display = 'block';
})();
