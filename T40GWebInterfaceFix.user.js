// ==UserScript==
// @name         T40G Webinterface fix
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  fixes Loginmask of T40G telephone!
// @author       You
// @updateURL    https://gitlab.com/Saiga/tampermonkeyscripts/raw/master/T40GWebInterfaceFix.user.js
// @downloadURL  https://gitlab.com/Saiga/tampermonkeyscripts/raw/master/T40GWebInterfaceFix.user.js
// @match        *://192.168.77.42/*
// @match        *://192.168.77.41/*
// @match        *://192.168.77.70/*
// @match        *://192.168.77.81/*
// @match        *://192.168.77.87/*
// @match        *://192.168.77.167/*
// @match        *://192.168.77.96/*
// @match        *://192.168.77.53/*
// @match        *://192.168.77.106/*
// @match        *://192.168.77.74/*
// @match        *://192.168.77.39/*
// @match        *://192.168.77.71/*
// @match        *://192.168.77.80/*
// @match        *://192.168.77.47/*
// @match        *://192.168.77.108/*
// @match        *://192.168.77.42/*
// @match        *://192.168.77.98/*
// @match        *://192.168.77.93/*
// @match        *://192.168.77.85/*
// @match        *://192.168.77.88/*
// @match        *://192.168.77.40/*
// @match        *://192.168.77.78/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=yealink.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var ajaxSucceedCallBackOrig = ajaxSucceedCallBack; // eslint-disable-line no-undef
    ajaxSucceedCallBack = function (req, res) { // eslint-disable-line no-undef
        if (/^[-\w,]+$/.test(res)) {
            res = res.split(",");
        }
        ajaxSucceedCallBackOrig(req, res);
    }
})();
